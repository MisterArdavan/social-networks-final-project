import networkx as nx
import numpy as np
import operator
from operator import itemgetter
import math
import matplotlib.pyplot as plt
import csv

TOP_TWENTY = 20


def get_explored_graph(graph):
    unexplored_nodes = [name for name, data in graph.nodes(data=True) if not data['explored']]
    print("number of explored nodes: " + str(nx.number_of_nodes(graph) - len(unexplored_nodes)))
    graph.remove_nodes_from(unexplored_nodes)


def network_stat(graph):
    print("number of nodes: " + str(len(graph.nodes())))
    print("number of edges: " + str(len(graph.edges())))
    print("density: " + str(nx.density(graph)))
    degrees = list(value[1] for value in nx.degree(graph))
    print("average degree: " + str(sum(list(degrees))/len(degrees)))
    # print("average path length: "+ str(nx.average_shortest_path_length(graph)))


def degs(graph, name, has_weight=False):
    degrees = graph.degree() if not has_weight else graph.degree(
        weight='weight')
    explored_nodes = [name for name, data in graph.nodes(data=True) if  data['explored']]
    # print(explored_nodes)
    deg_list = list()
    for value in degrees:
        if (value[0] in explored_nodes):
            deg_list.append(value[1])
    print(len(deg_list))

    # degs_sorted = sorted([value[1] for value in degrees], reverse=True)
    degs_sorted = sorted(deg_list, reverse=True)
    plt.loglog(degs_sorted, linestyle="", marker=".")
    plt.title(
        'Degree Distribution' if not has_weight else 'Weighted Degree Distribution')
    plt.xlabel('Nodes')
    plt.ylabel('Degree')
    plt.savefig('degrees' + name + '.png' if not has_weight else 'weighted-degrees.png')
    plt.clf()


def top_nodes(graph):
    names = nx.get_node_attributes(graph, 'name')
    nodes = sorted(graph.degree(), key=itemgetter(1), reverse=True)
    print(nodes)
    with open('top_nodes_1.csv', 'w', encoding='UTF-8') as out_file:
        writer = csv.writer(out_file)
        for i in range(TOP_TWENTY):
            row = [names[str(nodes[i][0])], nodes[i][1]]
            writer.writerow(row)


def clustering_coefficient(graph):
    names = nx.get_node_attributes(graph, 'name')
    # nodes = sorted([node for node in nx.clustering(graph, graph.nodes).values()],reverse=True)
    nodes = sorted(nx.clustering(graph, graph.nodes).items(), key = itemgetter(1), reverse=True)
    print(nodes)
    plt.plot(sorted([node for node in nx.clustering(graph, graph.nodes).values()],reverse=True),
        linestyle="", marker=".")
    with open('top_clustering_coefficient.csv', 'w', encoding='UTF-8') as out_file:
        writer = csv.writer(out_file)
        for i in range(30):
            row = [names[str(nodes[i][0])], nodes[i][1]]
            writer.writerow(row)
    plt.title('Clustering Coefficient Distribution')
    plt.xlabel('Nodes')
    plt.ylabel('Clustering Coefficient')
    plt.savefig('clustering-coefficient-distribution')
    plt.clf()
    print('Average Clustering Coefficient: ',
          nx.average_clustering(graph, graph.nodes))


def connected_components(graph):
    # c_list = (graph.subgraph(c) for c in nx.connected_components(graph))
    print("Number of Connected Components: " + str(
        nx.number_connected_components(graph)))


def degree_centrality(graph):
    centrality = nx.degree_centrality(graph)
    sorted_centrality = sorted((value for key, value in centrality.items()),
                               reverse=True)
    degree_sequence = sorted([d for n, d in graph.degree()], reverse=True)
    dmax = max(degree_sequence)
    n = nx.number_of_nodes(graph)
    numerator = sum(dmax - d for n, d in graph.degree())
    normalized_degree_centrality = numerator / ((n - 1) * (n - 2))
    print('Normalized Degree Centrality for Graph: ' + str(
        normalized_degree_centrality))
    plt.plot(sorted_centrality, linestyle="", marker=".")
    plt.title('Degree Centrality Distribution')
    plt.xlabel('Nodes')
    plt.ylabel('Degree Centrality')
    plt.savefig('degree centrality.png')
    plt.clf()


def betweenness_centrality(graph, name):
    centrality = nx.betweenness_centrality(graph)
    sorted_centrality = sorted((value for key, value in centrality.items()),
                               reverse=True)
    # print('Top Nodes: Betweenness Centrality for Graph: ' + str(
    # 	sorted_centrality[:19]))
    plt.loglog(sorted_centrality, linestyle="", marker=".")
    plt.title('Betweenness Centrality Distribution')
    plt.xlabel('Nodes')
    plt.ylabel('Betweenness Centrality')
    plt.savefig('betweenness_centrality' + name + '.png')
    plt.clf()


def top_books(graph):
    bipartite = nx.get_node_attributes(graph, 'bipartite')
    name = nx.get_node_attributes(graph, 'name')

    books = list()
    for k, v in bipartite.items():
        if v == 1:
            books.append(k)
    degrees = {}
    for book in books:
        degrees[book] = graph.degree[book]

    sorted_degrees = sorted(degrees.items(), key=operator.itemgetter(1), reverse=True)

    with open('top_books.csv', 'w', encoding='UTF-8') as out_file:
        writer = csv.writer(out_file)
        for i in range(TOP_TWENTY):
            row = str(sorted_degrees[i][1]), str(name[sorted_degrees[i][0]])
            writer.writerow(row)

def eigenvector_centrality(graph):
    centrality = nx.eigenvector_centrality(graph)
    sorted_centrality = sorted((value for key, value in centrality.items()),
                               reverse=True)
    plt.plot(sorted_centrality, linestyle="", marker=".")
    plt.title('Eigenvector Centrality Distribution')
    plt.xlabel('Nodes')
    plt.ylabel('Eigenvector Centrality')
    plt.savefig('eigenvector_centrality.png')
    plt.clf()



def main():
    friendship_graph = nx.read_gexf('friendship_graph.latest.gexf')
    # book_graph = nx.read_gexf('user_book_graph.latest.gexf')
    network_stat(friendship_graph)
    fg_copy = friendship_graph.copy()
    get_explored_graph(fg_copy)
    network_stat(fg_copy)

    # degs(friendship_graph, 'friendship')
    # top_nodes(friendship_graph)
    # clustering_coefficient(friendship_graph)
    # connected_components(friendship_graph)
    # degree_centrality(friendship_graph)
    betweenness_centrality(friendship_graph,' all nodes')
    betweenness_centrality(fg_copy,'explored')
    # eigenvector_centrality(friendship_graph)
    # top_books(book_graph)


# nx.draw(friendship_graph, pos=nx.spring_layout(friendship_graph))
# plt.savefig('graph.png')


if __name__ == "__main__":
    main()
